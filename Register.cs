//Open–closed principle SOLID-a, clasa je otvorena za koristenje (public), ali su svi elementi private tipa, odnosno, ne moze im se pristupiti van klase
public class User {
    private int userID;
    private String userFirstName;
    private String userLastName;
    private int userOIB;
    private bool is_event_owner;
    private String user_email;
    private int creditCardNumber;
    
    public User (String userFirstName, String userLastName, int userID) {
        this.getUserID(userID);
        this.setUserNameAndLastName(userFirstName, userLastName);
    }
    public User (String user_email, int userID) {
        this.getUserID(userID);
        this.setUserEmail(user_email);
    }
    public User (int creditCardNumber, int userID) {
        this.getUserID(userID);
        this.setUserCreditCardNum(setUserCreditCardNum);
    }
    public User (int userOIB, int userID) {
        this.getUserID(userID);
        this.setUserOIB(userOIB);
    }
    public String getUser() {
        return String.Format("{0}, {1}, {2}, {3}", userLastName, userFirstName, user_email, is_event_owner);
    }
    public void setUser(String userFirstName, String userLastName) {
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
    }
    public void setUserCreditCardNum(int creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }
}


public class User {
    private int adminID;
    private String userName;
    private String admin_password;
    private bool is_admin;
    private String admin_email;
    
    public Admin (String userName, int adminID) {
        this.getAdminID(adminID);
        this.setAdminName(userName);
    }
    
    public Admin (String admin_email, int adminID) {
        this.getAdminID(adminID);
        this.setAdminEmail(admin_email);
    }

    public setAdminPassword(String admin_password) {
        this.admin_password = admin_password;
    }
}


public class Register {
    private List<User> users;

    public Users() {
        users = new ArrayList<User>();
    }
    public List<User> getUser(){
        return this.users;
    }
    public void addUser(User user) {
        try
        {
            users.Add(user);    
        }
        catch (Exception e)
        {
            throw e.Message("User allready exists, please change userName");
        }
    }
    public void removeItemByCode(int userID){
        try
        {
            users.DeleteUser(userID);    
        }
        catch (Exception e)
        {
            throw e.Message.Format("User with userID: {0} does not exist in list.", userID);
        }
    }
}