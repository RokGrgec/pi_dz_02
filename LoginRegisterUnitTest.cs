// Login Tests
[Test]
public void ShouldAuthenticateValidUser()
{
    IMyMockDa mockDa = new MockDataAccess();
    var service = new AuthenticationService(mockDa);

    mockDa.AddUser("Name", "Password");

    Assert.IsTrue(service.DoLogin("Name", "Password"));

    //Ensure data access layer was used
    Assert.IsTrue(mockDa.GetUserFromDBWasCalled);
}

[Test]
public void ShouldNotAuthenticateUserWithInvalidPassword()
{
    IMyMockDa mockDa = new MockDataAccess();
    var service = new AuthenticationService(mockDa);

    mockDa.AddUser("Name", "Password");

    Assert.IsFalse(service.DoLogin("Name", "BadPassword"));

    //Ensure data access layer was used
    Assert.IsTrue(mockDa.GetUserFromDBWasCalled);
}

// Register Tests
[Test]
public void Edit_CreateNewItem_UserIdAssignedinModel()
{
    // 1 - Arrange
    int userID = 2;
    var mockData = MockUsers.MockUser();
    var modTwoUser = new ItemController(mockData.Object, userID); // Create a controller for the user with userID=2.

    // 2 - Act
    var actionResult = (ViewResult)modTwoUser.Edit(); // Call the edit view with no item Id (Add New).

    // 3 - Assert
    var userModel = (Item)actionResult.Model;
    Assert.IsTrue(userModel != null && userModel.UserId == UserId);
}
[Test]
public void DeleteUser_test()
{

    int userID = 2;
    var mockData = MockUsers.MockUser();

    var actionResult = (ViewResult)modTwoUser.Edit(); // Call the edit view with no item Id (Add New).

    var userModel = (Item)actionResult.Model;
    Assert.IsTrue(userModel != null && userModel.UserId == UserId);
}

// Adding New Admin Test
[TestMethod]

public void Adding_10_And_15_Returns_25()
{
    var admin = new Admin();
    int result = admin.Add("AdminName", "AdminPassword");
    Assert.AddNewAdmin<Admin>("AdminName", "AdminPassword", result);
}

